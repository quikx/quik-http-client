package org.fxinsiders.trade.quik;

import java.util.Iterator;
import java.util.NoSuchElementException;


/**
 *
 */
public interface Orders extends Iterable<Order> {
  Orders NO_ORDERS = () -> new Iterator<Order>() {
    @Override
    public boolean hasNext() {
      return false;
    }

    @Override
    public Order next() {
      throw new NoSuchElementException();
    }
  };
}
