package org.fxinsiders.trade.quik;

/**
 *
 */
public class QUIKException extends RuntimeException {
  public QUIKException(Throwable cause) {
    super(cause);
  }

  public QUIKException(String message, Throwable cause) {
    super(message, cause);
  }
}
