package org.fxinsiders.trade.quik;

/**
 *
 */
public enum StopStatus {
  /**
   * Заявка только что зарегистрирована и ещё не попала в таблицу заявок квика
   */
  NOT_FOUND,

  ACTIVE,

  EXECUTED,

  CANCELLED,

  REJECTED
}