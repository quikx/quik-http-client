package org.fxinsiders.trade.quik.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import si.mazi.rescu.Interceptor;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.net.ConnectException;


/**
 *
 */
final class ConnectionLogger implements Interceptor {
  private final Logger log = LoggerFactory.getLogger("quik");

  private volatile boolean connectionLost = false;

  @Override
  public Object aroundInvoke(InvocationHandler handler, Object proxy, Method method, Object[] args) throws Throwable {
    try {
      Object response = handler.invoke(proxy, method, args);
      markConnected();
      return response;
    }
    catch (ConnectException e) {
      markDisconnected();
      throw e;
    }
  }

  private void markConnected() {
    if (connectionLost)
      synchronized (this) {
        if (connectionLost)
          log.info("Связь с QUIK восстановлена");
        connectionLost = false;
      }
  }

  private void markDisconnected() {
    if (!connectionLost)
      synchronized (this) {
        if (!connectionLost)
          log.warn("Связь с QUIK потеряна");
        connectionLost = true;
      }
  }
}
