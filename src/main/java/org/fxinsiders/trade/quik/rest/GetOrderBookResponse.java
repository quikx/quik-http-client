package org.fxinsiders.trade.quik.rest;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 *
 */
final class GetOrderBookResponse {
  @JsonProperty(required = true)
  List<GetAskResponse> asks;

  @JsonProperty(required = true)
  List<GetBidResponse> bids;
}
