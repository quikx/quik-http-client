package org.fxinsiders.trade.quik.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.fxinsiders.trade.market.Price;

import static org.apache.commons.lang3.builder.ToStringStyle.NO_CLASS_NAME_STYLE;


/**
 *
 */
final class GetTradeResponse {
  @SuppressWarnings("WeakerAccess")
  @JsonProperty(value = "trade_num", required = true)
  String number;

  @JsonProperty(value = "flags", required = true)
  long flags;

  @JsonDeserialize(using = PriceDeserializer.class)
  @JsonProperty(value = "price", required = true)
  Price price;

  @JsonProperty(value = "qty", required = true)
  int quantity;

  @Override
  public String toString() {
    return new ToStringBuilder(this, NO_CLASS_NAME_STYLE)
      .append("num", number)
      .append("flags", flags)
      .append("qty", quantity)
      .append("price", price)
      .toString();
  }
}
