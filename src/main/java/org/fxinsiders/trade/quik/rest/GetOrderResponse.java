package org.fxinsiders.trade.quik.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.fxinsiders.trade.market.Price;

import static org.apache.commons.lang3.builder.ToStringStyle.NO_CLASS_NAME_STYLE;


/**
 *
 */
final class GetOrderResponse {
  @JsonProperty(value = "order_num", required = true)
  String number;

  @JsonProperty(value = "flags", required = true)
  long flags;

  @JsonProperty(value = "ext_order_status")
  int extOrderStatus;

  @JsonProperty(value = "account", required = true)
  String account;

  @JsonProperty(value = "client_code")
  String clientCode;

  @JsonProperty(value = "class_code", required = true)
  String classCode;

  @JsonProperty(value = "sec_code", required = true)
  String securityCode;

  @JsonProperty(value = "qty", required = true)
  int quantity;

  @JsonDeserialize(using = PriceDeserializer.class)
  @JsonProperty(value = "price")
  Price price;

  @Override
  public String toString() {
    return new ToStringBuilder(this, NO_CLASS_NAME_STYLE)
      .append(number)
      .append("flags", flags)
      .append("extOrderStatus", extOrderStatus)
      .toString();
  }
}
