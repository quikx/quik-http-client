package org.fxinsiders.trade.quik.rest;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 *
 */
final class PutOrderResponse {
  @JsonProperty(value = "order_num", required = true)
  String number;

  @JsonProperty(value = "trans_id", required = true)
  String transaction;
}
