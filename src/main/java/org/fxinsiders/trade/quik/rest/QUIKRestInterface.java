package org.fxinsiders.trade.quik.rest;

import org.fxinsiders.trade.market.Price;

import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import java.io.IOException;
import java.util.List;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.TEXT_PLAIN;


/**
 *
 */
@Path("/")
interface QUIKRestInterface {
  /**
   * Заглушка на тело запроса
   * Используется для подавления бага с зависанием PUT, POST, DELETE
   * (PUT, POST, DELETE зависают, если у запроса нет тела)
   */
  String STUB = "{}";

  ////////////////////////////////////////////////////////////////////////////

  @GET
  @Produces(TEXT_PLAIN)
  @Path("/ping")
  String ping() throws IOException;

  ////////////////////////////////////////////////////////////////////////////
  // Order Book

  @POST
  @Path("/subscribe/quotes")
  @Produces(APPLICATION_JSON)
  void subscribeQuotes(
    @QueryParam("class") String classCode,
    @QueryParam("security") String securityCode,
    @FormParam("stub") String stub
  ) throws IOException;

  @GET
  @Path("/order-book")
  @Produces(APPLICATION_JSON)
  GetOrderBookResponse getOrderBook(
    @QueryParam("class") String classCode,
    @QueryParam("security") String securityCode
  ) throws IOException;

  ////////////////////////////////////////////////////////////////////////////
  // Trade

  @GET
  @Path("/trades")
  @Produces(APPLICATION_JSON)
  List<GetTradeResponse> getTrades(@QueryParam("trans_id") String transaction) throws IOException;

  ////////////////////////////////////////////////////////////////////////////
  // Order

  @PUT
  @Path("/order")
  @Produces(APPLICATION_JSON)
  PutOrderResponse putOrder(
    @QueryParam("type") String type,
    @QueryParam("account") String account,
    @QueryParam("client_code") String clientCode,
    @QueryParam("class") String classCode,
    @QueryParam("security") String securityCode,
    @QueryParam("operation") String operationCode,
    @QueryParam("quantity") int quantity,
    @QueryParam("price") Price limit,
    @FormParam("body") String stub
  ) throws IOException;

  @GET
  @Path("/order")
  @Produces(APPLICATION_JSON)
  GetOrderResponse getOrderByNumber(@QueryParam("num") String number) throws IOException;

  @GET
  @Path("/orders")
  @Produces(APPLICATION_JSON)
  List<GetOrderResponse> getOrders(@QueryParam("trans_id") String transaction) throws IOException;

  @DELETE
  @Path("/order")
  void killOrder(
    @QueryParam("class") String classCode,
    @QueryParam("security") String securityCode,
    @QueryParam("order_key") String orderNumber,
    @FormParam("stub") String stub
  ) throws IOException;

  ////////////////////////////////////////////////////////////////////////////
  // Stop

  @PUT
  @Path("/stop-order")
  @Produces(APPLICATION_JSON)
  PutOrderResponse putStop(
    @QueryParam("stop_order_kind") String stopOrderKind,
    @QueryParam("account") String account,
    @QueryParam("client_code") String clientCode,
    @QueryParam("class") String classCode,
    @QueryParam("security") String securityCode,
    @QueryParam("operation") String operationCode,
    @QueryParam("quantity") int quantity,
    @QueryParam("stop_price") Price stop,
    @QueryParam("price") Price limit,
    @QueryParam("linked_order_price") Price linked,
    @FormParam("stub") String stub
  ) throws IOException;

  @GET
  @Path("/stop-order")
  @Produces(APPLICATION_JSON)
  GetStopResponse getStopByNumber(@QueryParam("num") String number) throws IOException;

  @DELETE
  @Path("/stop-order")
  void killStop(
    @QueryParam("class") String classCode,
    @QueryParam("security") String securityCode,
    @QueryParam("stop_order_key") String orderNumber,
    @FormParam("stub") String stub
  ) throws IOException;
}
