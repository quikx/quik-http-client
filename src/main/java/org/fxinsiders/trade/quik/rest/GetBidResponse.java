package org.fxinsiders.trade.quik.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.fxinsiders.trade.market.Price;

/**
 *
 */
final class GetBidResponse {
  @JsonDeserialize(using = PriceDeserializer.class)
  @JsonProperty(required = true)
  Price price;

  @JsonProperty(required = true)
  int quantity;
}
