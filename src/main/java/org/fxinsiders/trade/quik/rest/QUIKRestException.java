package org.fxinsiders.trade.quik.rest;

import org.fxinsiders.trade.quik.QUIKException;
import si.mazi.rescu.HttpStatusIOException;

import static org.apache.commons.lang3.StringUtils.defaultIfEmpty;


/**
 *
 */
final class QUIKRestException extends QUIKException {
  QUIKRestException(HttpStatusIOException e) {
    super(defaultIfEmpty(e.getHttpBody(), e.getMessage()), e);
  }

  QUIKRestException(String message, Throwable cause) {
    super(message, cause);
  }

  QUIKRestException(Throwable cause) {
    super(cause);
  }
}
