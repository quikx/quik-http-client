package org.fxinsiders.trade.quik.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.fxinsiders.trade.market.Ask;
import org.fxinsiders.trade.market.Asks;
import org.fxinsiders.trade.market.Bid;
import org.fxinsiders.trade.market.Bids;
import org.fxinsiders.trade.market.Instrument;
import org.fxinsiders.trade.market.OrderBook;
import org.fxinsiders.trade.market.Price;
import org.fxinsiders.trade.market.Side;
import org.fxinsiders.trade.quik.Order;
import org.fxinsiders.trade.quik.OrderProps;
import org.fxinsiders.trade.quik.OrderStatus;
import org.fxinsiders.trade.quik.OrderType;
import org.fxinsiders.trade.quik.Orders;
import org.fxinsiders.trade.quik.QUIKClient;
import org.fxinsiders.trade.quik.Stop;
import org.fxinsiders.trade.quik.StopProps;
import org.fxinsiders.trade.quik.StopStatus;
import org.fxinsiders.trade.quik.Trades;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import si.mazi.rescu.ClientConfig;
import si.mazi.rescu.HttpStatusIOException;
import si.mazi.rescu.RestProxyFactory;
import si.mazi.rescu.serialization.jackson.DefaultJacksonObjectMapperFactory;

import java.io.IOException;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeoutException;

import static com.fasterxml.jackson.databind.DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT;
import static com.fasterxml.jackson.databind.DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT;
import static com.fasterxml.jackson.databind.DeserializationFeature.ACCEPT_FLOAT_AS_INT;
import static com.fasterxml.jackson.databind.DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE;
import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_INVALID_SUBTYPE;
import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES;
import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;
import static com.fasterxml.jackson.databind.DeserializationFeature.READ_DATE_TIMESTAMPS_AS_NANOSECONDS;
import static com.fasterxml.jackson.databind.DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS;
import static com.fasterxml.jackson.databind.DeserializationFeature.WRAP_EXCEPTIONS;
import static java.lang.Thread.sleep;
import static java.time.Duration.ofMillis;
import static java.time.Duration.ofSeconds;
import static java.time.LocalDateTime.now;
import static java.util.Objects.requireNonNull;
import static org.fxinsiders.trade.market.Price.ZERO_PRICE;
import static org.fxinsiders.trade.market.Side.BUY;
import static org.fxinsiders.trade.market.Side.SELL;
import static org.fxinsiders.trade.quik.OrderType.LIMIT;
import static org.fxinsiders.trade.quik.OrderType.MARKET;
import static org.fxinsiders.trade.quik.Orders.NO_ORDERS;
import static org.fxinsiders.trade.quik.Trades.NO_TRADES;
import static org.fxinsiders.trade.quik.rest.QUIKRestInterface.STUB;


/**
 *
 */
public final class QUIKRestClient implements QUIKClient {
  private static final URL DEFAULT_URL = urlFromString("http://localhost:8080");
  private static final Duration DEFAULT_TIMEOUT = ofSeconds(5);
  private static final Duration WAIT_DELAY = ofMillis(50);

  private final Logger log = LoggerFactory.getLogger("quik");
  private final QUIKRestInterface restInterface;
  private final Duration timeout;
  private final URL url;

  public QUIKRestClient() {
    this(DEFAULT_URL, DEFAULT_TIMEOUT);
  }

  public QUIKRestClient(URL url, Duration timeout) {
    this.url = requireNonNull(url);
    this.timeout = requireNonNull(timeout);

    ClientConfig rescuConfig = new ClientConfig();
    rescuConfig.setJacksonObjectMapperFactory(
      new DefaultJacksonObjectMapperFactory() {
        @Override
        public void configureObjectMapper(ObjectMapper mapper) {
          super.configureObjectMapper(mapper);
          mapper.configure(WRAP_EXCEPTIONS, false);
          mapper.configure(ACCEPT_FLOAT_AS_INT, false);
          mapper.configure(FAIL_ON_INVALID_SUBTYPE, false);
          mapper.configure(USE_BIG_DECIMAL_FOR_FLOATS, true);
          mapper.configure(FAIL_ON_UNKNOWN_PROPERTIES, false);
          mapper.configure(FAIL_ON_NULL_FOR_PRIMITIVES, true);
          mapper.configure(ADJUST_DATES_TO_CONTEXT_TIME_ZONE, false);
          mapper.configure(ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
          mapper.configure(ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT, true);
          mapper.configure(READ_DATE_TIMESTAMPS_AS_NANOSECONDS, false);
        }
      }
    );
    rescuConfig.setHttpConnTimeout((int) timeout.toMillis());
    rescuConfig.setHttpReadTimeout((int) timeout.toMillis());

    restInterface = RestProxyFactory.createProxy(
      QUIKRestInterface.class,
      url.toExternalForm(),
      rescuConfig,
      new ConnectionLogger()
    );
  }

  @Override
  public URL url() {
    return url;
  }

  @Override
  public Duration timeout() {
    return timeout;
  }

  ////////////////////////////////////////////////////////////////////////////

  @Override
  public boolean ping() {
    try {
      String response = restInterface.ping();
      if (!response.startsWith("QUIK")) {
        log.info("QUIK не найден по адресу {}: {}", url, response);
        return false;
      }
      return true;
    }
    catch (ConnectException e) {
      log.info("QUIK не отвечает по адресу {}: {}", url, e);
      return false;
    }
    catch (IOException e) {
      log.error("Сбой QUIK ping", e);
      return false;
    }
  }

  /////////////////////////////////////////////////////////////////////////////

  @Override
  public OrderBook getOrderBook(Instrument instrument) {
    String classCode = parseInstrument(instrument).get(0);
    String securityCode = parseInstrument(instrument).get(1);

    try {
      GetOrderBookResponse response = restInterface.getOrderBook(classCode, securityCode);

      List<Ask> asks = new ArrayList<>(response.asks.size());
      for (GetAskResponse r : response.asks)
        asks.add(new Ask(r.price, r.quantity));

      List<Bid> bids = new ArrayList<>(response.bids.size());
      for (GetBidResponse r : response.bids)
        bids.add(new Bid(r.price, r.quantity));

      return new OrderBook(
        instrument,
        LocalDateTime.now(),
        new Asks(asks),
        new Bids(bids)
      );
    }
    catch (IOException e) {
      throw new QUIKRestException(
        MessageFormat.format("Сбой на получении стакана котировок {0}: {1}", instrument, getMessage(e)), e);
    }
  }

  @Override
  public void subscribeQuotes(Instrument instrument) {
    String classCode = parseInstrument(instrument).get(0);
    String securityCode = parseInstrument(instrument).get(1);

    try {
      restInterface.subscribeQuotes(classCode, securityCode, STUB);
    }
    catch (IOException e) {
      throw new QUIKRestException(
        MessageFormat.format("Сбой подписки на котировки {0}: {1}", instrument, getMessage(e)), e);
    }
  }

  /////////////////////////////////////////////////////////////////////////////

  @Override
  public Order placeOrder(OrderProps props) {
    requireNonNull(props);
    try {
      PutOrderResponse response;
      switch (props.type()) {
        case MARKET:
          response = restInterface.putOrder(
            "market",
            props.account(),
            props.clientCode(),
            props.classCode(),
            props.securityCode(),
            props.side() == BUY ? "B" : "S",
            props.quantity(),
            null,
            STUB
          );
          break;

        case LIMIT:
          response = restInterface.putOrder(
            "limit",
            props.account(),
            props.clientCode(),
            props.classCode(),
            props.securityCode(),
            props.side() == BUY ? "B" : "S",
            props.quantity(),
            props.price(),
            STUB
          );
          break;

        default:
          throw new UnsupportedOperationException("Неожиданный тип заявки " + props.type());
      }
      Order order = new Order(response.number, response.transaction, props);
      log.info("Заявка {} успешно отправлена в QUIK", order);
      return order;
    }
    catch (IOException e) {
      throw new QUIKRestException(
        MessageFormat.format("Сбой на отправке заявки {0}: {1}", props, getMessage(e)), e);
    }
  }

  @Override
  public Stop placeStop(StopProps props) {
    requireNonNull(props);
    try {
      PutOrderResponse response;
      switch (props.kind()) {
        case SIMPLE_STOP:
          response = restInterface.putStop(
            "simple_stop_order",
            props.account(),
            props.clientCode(),
            props.classCode(),
            props.securityCode(),
            props.side() == BUY ? "B" : "S",
            props.quantity(),
            props.stopPrice(),
            props.limitPrice(),
            null,
            STUB
          );
          break;

        case STOP_WITH_LINKED_ORDER:
          response = restInterface.putStop(
            "with_linked_limit_order",
            props.account(),
            props.clientCode(),
            props.classCode(),
            props.securityCode(),
            props.side() == BUY ? "B" : "S",
            props.quantity(),
            props.stopPrice(),
            props.limitPrice(),
            props.linkedOrderPrice(),
            STUB
          );
          break;

        default:
          throw new UnsupportedOperationException("Неожиданный вид стопа " + props.kind());
      }
      Stop stop = new Stop(response.number, response.transaction, props);
      log.info("Стоп {} успешно отправлен в QUIK", stop);
      return stop;
    }
    catch (IOException e) {
      throw new QUIKRestException(
        MessageFormat.format("Сбой на отправке стопа {0}: {1}", props, getMessage(e)), e);
    }
  }

  ////////////////////////////////////////////////////////////////////////////

  @Override
  public Trades waitTrades(Order order, LocalDateTime deadline) throws InterruptedException, TimeoutException {
    return waitTrades(order.transaction(), order.quantity(), deadline);
  }

  @Override
  public Trades waitTrades(Stop stop, LocalDateTime deadline) throws InterruptedException, TimeoutException {
    return waitTrades(stop.transaction(), stop.quantity(), deadline);
  }

  private Trades waitTrades(String transaction, int target, LocalDateTime deadline) throws InterruptedException, TimeoutException {
    requireNonNull(deadline);
    while (true) {
      Trades trades = getTrades(transaction);
      if (trades.quantity() >= target)
        return trades;
      if (now().isAfter(deadline)) {
        throw new TimeoutException();
      }
      sleep(WAIT_DELAY.toMillis());
    }
  }

  ////////////////////////////////////////////////////////////////////////////

  @Override
  public void cancelOrder(Order order) {
    requireNonNull(order);
    try {
      restInterface.killOrder(order.classCode(), order.securityCode(), order.number(), STUB);
      log.info("Заявка {} снята", order);
    }
    catch (HttpStatusIOException e) {
      try {
        if (e.getHttpStatusCode() == 400) {
          OrderStatus status = getOrderStatus(order);
          if (status != OrderStatus.ACTIVE) {
            log.warn("Заявка {} находится в статусе {} и не может быть снята", order, status);
            return;
          }
        }
      }
      catch (Exception e2) {
        e.addSuppressed(e2);
      }
      throw new QUIKRestException(
        MessageFormat.format("Сбой на снятии заявки {0}: {1}", order, getMessage(e)), e);
    }
    catch (IOException e) {
      throw new QUIKRestException(
        MessageFormat.format("Сбой на снятии заявки {0}: {1}", order, getMessage(e)), e);
    }
  }

  @Override
  public void cancelStop(Stop stop) {
    requireNonNull(stop);
    try {
      StopStatus status = getStopStatus(stop);
      if (status != StopStatus.ACTIVE) {
        log.warn("Стоп {} находится в статусе {} и  не может быть снят", stop, status);
        return;
      }
      restInterface.killStop(stop.classCode(), stop.securityCode(), stop.number(), STUB);
      log.info("Стоп {} снят", stop);
    }
    catch (HttpStatusIOException e) {
      try {
        if (e.getHttpStatusCode() == 400) {
          StopStatus status = getStopStatus(stop);
          if (status != StopStatus.ACTIVE) {
            log.warn("Стоп {} находится в статусе {} и  не может быть снят", stop, status);
            return;
          }
        }
      }
      catch (Exception e2) {
        e.addSuppressed(e2);
      }
      throw new QUIKRestException(
        MessageFormat.format("Сбой на снятии стопа {0}: {1}", stop, getMessage(e)), e);
    }
    catch (IOException e) {
      throw new QUIKRestException(
        MessageFormat.format("Сбой на снятии стопа {0}: {1}", stop, getMessage(e)), e);
    }
  }

  @Override
  public void cancelStopWithOrders(Stop stop) {
    requireNonNull(stop);

    cancelStop(stop);

    Orders orders = getOrders(stop.transaction());
    for (Order order : orders)
      cancelOrder(order);
  }

  private OrderStatus getOrderStatus(Order order) {
    requireNonNull(order);
    try {
      GetOrderResponse response = restInterface.getOrderByNumber(order.number());
      if (response == null)
        return OrderStatus.NOT_FOUND;

      log.debug("Получен статус заявки {}: {}", order, response.flags);
      log.debug("Получен расширенный статус заявки {}: {}", order, response.extOrderStatus);
      switch (response.extOrderStatus) {
        case 1:
        case 2:
          return OrderStatus.ACTIVE;
        case 3:
          return OrderStatus.EXECUTED;
        case 4:
          return OrderStatus.CANCELLED;
        case 7:
          return OrderStatus.REJECTED;
        case 10:
          return OrderStatus.EXPIRED;
        default:
          if ((response.flags & 0x01) != 0)
            return OrderStatus.ACTIVE;
          return ((response.flags & 0x2) == 0)
            ? OrderStatus.EXECUTED
            : OrderStatus.CANCELLED;
      }
    }
    catch (IOException e) {
      throw new QUIKRestException(
        MessageFormat.format("Сбой на проверке статуса заявки {0}: {1}", order, getMessage(e)), e);
    }
  }

  private StopStatus getStopStatus(Stop stop) {
    requireNonNull(stop);
    try {
      GetStopResponse response = restInterface.getStopByNumber(stop.number());
      if (response == null)
        return StopStatus.NOT_FOUND;

      log.debug("Получен статус стопа {}: {}", stop, response.flags);
      if ((response.flags & 0x01) != 0)
        return StopStatus.ACTIVE;
      if ((response.flags & (0x01 | 0x2)) == 0)
        return StopStatus.EXECUTED;
      if ((response.flags & (0x400 | 0x800)) != 0)
        return StopStatus.REJECTED;
      if ((response.flags & 0x2) != 0)
        return StopStatus.CANCELLED;
      return StopStatus.ACTIVE;

    }
    catch (IOException e) {
      throw new QUIKRestException(
        MessageFormat.format("Сбой на проверке статуса стопа {0}: {1}", stop, getMessage(e)), e);
    }
  }

  ////////////////////////////////////////////////////////////////////////////

  @Override
  public Trades getTrades(Order order) {
    return getTrades(order.transaction());
  }

  @Override
  public Trades getTrades(Stop stop) {
    return getTrades(stop.transaction());
  }

  private Trades getTrades(String transaction) {
    requireNonNull(transaction);
    try {
      List<GetTradeResponse> responses = restInterface.getTrades(transaction);
      if (responses.isEmpty())
        return NO_TRADES;

      log.info("По транзакции {} получены сделки {}", transaction, responses);

      int div = 0;
      Price num = ZERO_PRICE;
      for (GetTradeResponse response : responses) {
        num = num.plus(response.price.mul(response.quantity));
        div += response.quantity;
      }

      Price price = (div > 0) ? num.div(div) : ZERO_PRICE;
      int quantity = div;
      return new Trades() {
        @Override
        public Price price() { return price; }

        @Override
        public int quantity() {
          return quantity;
        }

        public Side side() {
          Iterator<GetTradeResponse> it = responses.iterator();
          if (it.hasNext()) {
            long flags = it.next().flags;
            return ((flags & 0x04) == 0) ? BUY : SELL;
          }
          return BUY;
        }
      };
    }
    catch (IOException e) {
      throw new QUIKRestException(
        MessageFormat.format("Сбой на получении сделок по транзакции {0}: {1}", transaction, getMessage(e)), e);
    }
  }

  ////////////////////////////////////////////////////////////////////////////

  @Override
  public Orders getOrders(String transaction) {
    requireNonNull(transaction);
    try {
      List<GetOrderResponse> responses = restInterface.getOrders(transaction);
      if (responses.isEmpty())
        return NO_ORDERS;

      log.info("По транзакции {} получены заявки {}", transaction, responses);

      Iterator<GetOrderResponse> iterator = responses.iterator();
      return () -> new Iterator<Order>() {
        @Override
        public boolean hasNext() {
          return iterator.hasNext();
        }

        @Override
        public Order next() {
          GetOrderResponse response = iterator.next();
          return new Order(response.number, transaction, new OrderProps() {
            @Override
            public OrderType type() {
              return (response.flags & 0x8) == 0 ? MARKET : LIMIT;
            }

            @Override
            public String account() { return response.account; }

            @Override
            public String clientCode() { return response.clientCode; }

            @Override
            public String classCode() { return response.classCode; }

            @Override
            public String securityCode() { return response.securityCode;}

            public Side side() {
              return (response.flags & 0x4) == 0 ? BUY : SELL;
            }

            @Override
            public int quantity() { return response.quantity; }

            @Override
            public Price price() { return response.price; }
          });
        }
      };
    }
    catch (IOException e) {
      throw new QUIKRestException(
        MessageFormat.format("Сбой на получении заявок по транзакции {0}: {1}", transaction, getMessage(e)), e);
    }
  }

  private String getMessage(IOException e) {
    if (e instanceof HttpStatusIOException) {
      HttpStatusIOException e2 = (HttpStatusIOException) e;
      return MessageFormat.format("{0}(HTTP {1})", e2.getHttpBody(), e.getMessage());
    }
    return e.getMessage();
  }

  private static List<String> parseInstrument(Instrument instrument) {
    String name = instrument.name.toString();
    List<String> parts = Arrays.asList(name.split("/"));
    parts.removeIf(String::isEmpty);
    if (parts.size() != 2)
      throw new IllegalArgumentException(
        String.format("%s не соответствует виду CLASS_CODE/SECURITY_CODE", instrument));
    return parts;
  }

  private static URL urlFromString(String urlString) {
    try {
      return new URL(urlString);
    }
    catch (MalformedURLException e) {
      throw new IllegalArgumentException(e);
    }
  }
}