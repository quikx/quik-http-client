package org.fxinsiders.trade.quik.rest;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdScalarDeserializer;
import org.fxinsiders.trade.market.Price;

import java.io.IOException;

import static com.fasterxml.jackson.core.JsonTokenId.ID_NUMBER_FLOAT;
import static com.fasterxml.jackson.core.JsonTokenId.ID_NUMBER_INT;
import static com.fasterxml.jackson.core.JsonTokenId.ID_START_ARRAY;
import static com.fasterxml.jackson.core.JsonTokenId.ID_STRING;
import static org.fxinsiders.trade.market.Price.ZERO_PRICE;


/**
 *
 */
final class PriceDeserializer extends StdScalarDeserializer<Price> {
  public PriceDeserializer() {
    super(Price.class);
  }

  @Override
  public Object getEmptyValue(DeserializationContext ctx) {
    return ZERO_PRICE;
  }

  @Override
  public Price deserialize(JsonParser p, DeserializationContext ctx) throws IOException {
    // Copied from BigDecimalDeserializer.deserialize()
    switch (p.getCurrentTokenId()) {
      case ID_NUMBER_INT:
      case ID_NUMBER_FLOAT:
        return new Price(p.getDecimalValue());

      case ID_STRING:
        String text = p.getText().trim();

        // note: no need to call `coerce` as this is never primitive
        if (_isEmptyOrTextualNull(text)) {
          _verifyNullForScalarCoercion(ctx, text);
          return getNullValue(ctx);
        }

        _verifyStringForScalarCoercion(ctx, text);
        try {
          return new Price(text);
        }
        catch (IllegalArgumentException ignored) { }

        return (Price) ctx.handleWeirdStringValue(_valueClass, text, "not a valid representation");

      case ID_START_ARRAY:
        return _deserializeFromArray(p, ctx);
    }
    // Otherwise, no can do:
    return (Price) ctx.handleUnexpectedToken(_valueClass, p);
  }
}
