package org.fxinsiders.trade.quik;

/**
 *
 */
public enum OrderType {
  MARKET,
  LIMIT
}