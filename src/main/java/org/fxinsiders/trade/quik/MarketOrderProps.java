package org.fxinsiders.trade.quik;


import org.fxinsiders.trade.market.Price;

/**
 *
 */
public abstract class MarketOrderProps extends OrderProps {
  @Override
  public final OrderType type() {
    return OrderType.MARKET;
  }

  @Override
  public final Price price() {
    return null;
  }
}
