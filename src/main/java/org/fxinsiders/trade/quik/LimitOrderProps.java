package org.fxinsiders.trade.quik;

/**
 *
 */
public abstract class LimitOrderProps extends OrderProps {
  @Override
  public final OrderType type() {
    return OrderType.LIMIT;
  }
}
