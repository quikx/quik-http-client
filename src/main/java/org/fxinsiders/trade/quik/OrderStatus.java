package org.fxinsiders.trade.quik;

/**
 *
 */
public enum OrderStatus {
  /**
   * Заявка может быть не найдена по двум причинам:
   * A. Заявка только что зарегистрировалась и ещё не попала в таблицу заявок квика
   * B. Некорректные параметры поиска заявки
   */
  NOT_FOUND,

  ACTIVE,

  EXECUTED,

  CANCELLED,

  REJECTED,

  EXPIRED
}
