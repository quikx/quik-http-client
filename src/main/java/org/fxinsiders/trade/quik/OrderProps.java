package org.fxinsiders.trade.quik;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.fxinsiders.trade.market.Price;
import org.fxinsiders.trade.market.Side;

import static org.apache.commons.lang3.builder.ToStringStyle.NO_CLASS_NAME_STYLE;


/**
 * Свойства заявки
 */
public abstract class OrderProps {
  public abstract OrderType type();

  public abstract String account();

  public abstract String clientCode();

  public abstract String classCode();

  public abstract String securityCode();

  public abstract Side side();

  public abstract int quantity();

  /**
   * @return цена для type=LIMIT
   * Иначе null
   */
  public abstract Price price();

  @Override
  public String toString() {
    return new ToStringBuilder(this, NO_CLASS_NAME_STYLE)
      .append(type())
      .append(side())
      .append(classCode())
      .append(securityCode())
      .append(quantity())
      .append(price())
      .append("account", account())
      .append("clientCode", clientCode())
      .toString();
  }
}
