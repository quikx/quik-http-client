package org.fxinsiders.trade.quik;

import org.fxinsiders.trade.market.Instrument;
import org.fxinsiders.trade.market.OrderBook;

import java.net.URL;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.concurrent.TimeoutException;


/**
 *
 */
public interface QUIKClient {
  URL url();

  Duration timeout();

  boolean ping();

  void subscribeQuotes(Instrument instrument);

  OrderBook getOrderBook(Instrument instrument);

  Order placeOrder(OrderProps props);

  Stop placeStop(StopProps props);

  Trades waitTrades(Order order, LocalDateTime deadline) throws InterruptedException, TimeoutException;

  Trades waitTrades(Stop stop, LocalDateTime deadline) throws InterruptedException, TimeoutException;

  void cancelOrder(Order order);

  void cancelStop(Stop stop);

  void cancelStopWithOrders(Stop stop);

  Trades getTrades(Order order);

  Trades getTrades(Stop stop);

  Orders getOrders(String transaction);
}
