package org.fxinsiders.trade.quik;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.fxinsiders.trade.market.Price;
import org.fxinsiders.trade.market.Side;

import static org.apache.commons.lang3.builder.ToStringStyle.NO_CLASS_NAME_STYLE;


/**
 * Свойства заявки
 */
public abstract class StopProps {
  public abstract StopKind kind();

  public abstract String account();

  public abstract String clientCode();

  public abstract String classCode();

  public abstract String securityCode();

  public abstract Side side();

  public abstract int quantity();

  /**
   * @return цена стопа
   * Never null
   */
  public abstract Price stopPrice();

  /**
   * @return лимит цены
   * Never null
   */
  public abstract Price limitPrice();

  /**
   * @return цена связанной лимитной заявки для kind=STOP_WITH_LINKED_ORDER
   * Иначе null
   */
  public abstract Price linkedOrderPrice();

  @Override
  public String toString() {
    return new ToStringBuilder(this, NO_CLASS_NAME_STYLE)
      .append(kind())
      .append(side())
      .append(classCode())
      .append(securityCode())
      .append(quantity())
      .append("stop", stopPrice())
      .append("limit", limitPrice())
      .append("linked", linkedOrderPrice())
      .append("account", account())
      .append("clientCode", clientCode())
      .toString();
  }
}