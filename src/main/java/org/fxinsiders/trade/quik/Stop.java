package org.fxinsiders.trade.quik;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.fxinsiders.trade.market.Side;

import static java.util.Objects.requireNonNull;
import static org.apache.commons.lang3.builder.ToStringStyle.NO_CLASS_NAME_STYLE;


/**
 * Заявка
 */
public final class Stop {
  private final String number;

  private final String transaction;

  private final StopProps props;

  public Stop(String number, String transaction, StopProps props) {
    this.number = requireNonNull(number);
    this.transaction = requireNonNull(transaction);
    this.props = requireNonNull(props);
  }

  public String number() {
    return number;
  }

  public String transaction() {
    return transaction;
  }

  public String classCode() {
    return props.classCode();
  }

  public String securityCode() {
    return props.securityCode();
  }

  public Side side() {
    return props.side();
  }

  public int quantity() {
    return props.quantity();
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this, NO_CLASS_NAME_STYLE)
      .append(number)
      .append(transaction)
      .append(props.toString())
      .toString();
  }
}
