package org.fxinsiders.trade.quik;

/**
 *
 */
public enum StopKind {
  SIMPLE_STOP,
  STOP_WITH_LINKED_ORDER
}
