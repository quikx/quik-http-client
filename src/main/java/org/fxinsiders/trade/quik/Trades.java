package org.fxinsiders.trade.quik;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.fxinsiders.trade.market.Price;
import org.fxinsiders.trade.market.Side;

import static org.apache.commons.lang3.builder.ToStringStyle.NO_CLASS_NAME_STYLE;
import static org.fxinsiders.trade.market.Price.ZERO_PRICE;
import static org.fxinsiders.trade.market.Side.BUY;


/**
 * Список сделок
 */
public abstract class Trades {
  /**
   * @return средняя цена по всем сделкам; 0 = сделок нет
   */
  public abstract Price price();

  /**
   * @return суммарное количество лотов по всем сделкам; 0 = сделок нет
   */
  public abstract int quantity();

  public abstract Side side();

  ////////////////////////////////////////////////////////////////////////////

  public Trades plus(Trades other) {
    if (this.quantity() == 0)
      return other;
    if (other.quantity() == 0)
      return this;

    if (this.side() != other.side())
      throw new IllegalArgumentException("Нельзя складывать разнонаправленные Trades");

    int total = this.quantity() + other.quantity();
    if (total > 0) {
      Price price = this.price().mul(this.quantity())
        .plus(other.price().mul(other.quantity()))
        .div(total);

      Side side = this.side();
      return new Trades() {
        public Price price() { return price; }

        public int quantity() { return total; }

        public Side side() { return side; }
      };
    }

    return NO_TRADES;
  }

  @Override
  public final String toString() {
    return new ToStringBuilder(this, NO_CLASS_NAME_STYLE)
      .append(side())
      .append("qty", quantity())
      .append("price", price())
      .toString();
  }

  public static final Trades NO_TRADES = new Trades() {
    @Override
    public Price price() {
      return ZERO_PRICE;
    }

    @Override
    public int quantity() {
      return 0;
    }

    @Override
    public Side side() {
      return BUY;
    }
  };
}