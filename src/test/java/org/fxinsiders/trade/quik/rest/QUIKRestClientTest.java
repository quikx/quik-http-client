package org.fxinsiders.trade.quik.rest;

import org.fxinsiders.trade.market.Instrument;
import org.fxinsiders.trade.market.OrderBook;
import org.fxinsiders.trade.market.Price;
import org.fxinsiders.trade.market.Side;
import org.fxinsiders.trade.quik.QUIKClient;
import org.fxinsiders.trade.quik.Stop;
import org.fxinsiders.trade.quik.StopKind;
import org.fxinsiders.trade.quik.StopProps;
import org.fxinsiders.trade.quik.Trades;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.concurrent.TimeoutException;

import static java.time.Duration.between;
import static java.time.Duration.ofSeconds;
import static java.time.LocalDateTime.now;
import static org.fxinsiders.trade.market.Amount.ZERO_AMOUNT;
import static org.fxinsiders.trade.market.Volume.ZERO_VOLUME;
import static org.fxinsiders.trade.quik.Trades.NO_TRADES;

/**
 *
 */
@Ignore
public final class QUIKRestClientTest {
  private final Logger log = LoggerFactory.getLogger("test");

  private final QUIKClient quik = new QUIKRestClient();
  private final String QJSIM = "QJSIM";
  private final String SBER = "SBER";
  private final Instrument sber =
    new Instrument(QJSIM + "/" + SBER, SBER, ZERO_VOLUME, ZERO_AMOUNT);

  @Test
  public void testPing() {
    Assert.assertTrue(quik.ping());
  }

  @Test
  public void testGetOrderBook() {
    log.info("Читаем стакан...");
    OrderBook orderBook = quik.getOrderBook(sber);
    log.info("Стакан = {}", orderBook);
    log.info("Лушая цена на покупку одного лота = {}", orderBook.getBestAskPrice(1));
    log.info("Лушая цена на продажу одного лота = {}", orderBook.getBestBidPrice(1));
  }

  @Test
  public void testBuyUsingStop() throws InterruptedException {
    final int quantity = 5;
    final double stopMargin = -0.05;
    final double limitMargin = -0.05;
    final Duration timeout = ofSeconds(20L);

    LocalDateTime start = now();
    log.info("Покупаем {} слотов простым стопом...", quantity);
    log.info("Стоп-цена = bestAsk + {}", stopMargin);
    log.info("Лимит-цена = bestAsk + {}", limitMargin);

    log.info("1. Смотрим стакан...");
    OrderBook orderBook = quik.getOrderBook(sber);
    log.info("Стакан = {}", orderBook);
    Price bestAskPrice = orderBook.getBestAskPrice(quantity);
    log.info("Лушая цена на покупку = {}", bestAskPrice);

    log.info("2. Выставляем стоп...");
    Price stopPrice = bestAskPrice.plus(stopMargin);
    Price limitPrice = bestAskPrice.plus(limitMargin);
    Stop stop = quik.placeStop(new StopProps() {
      @Override
      public StopKind kind() {
        return StopKind.SIMPLE_STOP;
      }

      @Override
      public String account() {
        return "NL0011100043";
      }

      @Override
      public String clientCode() {
        return "11100";
      }

      @Override
      public String classCode() {
        return QJSIM;
      }

      @Override
      public String securityCode() {
        return SBER;
      }

      @Override
      public Side side() {
        return Side.BUY;
      }

      @Override
      public int quantity() {
        return quantity;
      }

      @Override
      public Price stopPrice() {
        return stopPrice;
      }

      @Override
      public Price limitPrice() {
        return limitPrice;
      }

      @Override
      public Price linkedOrderPrice() {
        return null;
      }
    });

    log.info("3. Ждём сделки {} секунд...", timeout.getSeconds());
    Trades trades = NO_TRADES;
    LocalDateTime deadline = start.plus(timeout);
    try {
      while (trades.quantity() < quantity)
        trades = quik.waitTrades(stop, deadline);
    }
    catch (TimeoutException e) {
      log.warn("4. Покупка не удалась: снимаю стоп и заявки...");
      quik.cancelStopWithOrders(stop);
      Assert.fail("Покупка не удалась: время вышло");
      return;
    }

    log.info("Покупка выполнена за {} секунд", between(start, now()).getSeconds());
  }
}
